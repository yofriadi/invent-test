const app = require("express")();
const { MongoClient } = require("mongodb");
const client = require("redis").createClient();

const dbName = "backendenginer_nama_peserta";
const url = `mongodb://localhost:27017/${dbName}`;
const products = "products";
const variants = "variants";

(async function init() {
  try {
    const connection = await MongoClient.connect(url, {
      useUnifiedTopology: true
    });
    const db = connection.db(dbName);
    console.log(`Database ${dbName} connect!`);

    const [Product, Variant] = await Promise.all([
      db.createCollection(products),
      db.createCollection(variants)
    ]);

    await Product.insertMany([
      {
        uid: "dn1",
        name: "baju muslim",
        category: "baju",
        status: true,
        type: "ecommerce"
      },
      {
        uid: "dn2",
        name: "baju renang",
        category: "baju",
        status: false,
        type: "ecommerce"
      },
      {
        uid: "dn3",
        name: "baju batik",
        category: "baju",
        status: true,
        type: "ecommerce"
      }
    ]).then(({ insertedIds }) => {
      const { 0: productId1, 1: productId2, 2: productId3 } = insertedIds;

      return Variant.insertMany([
        {
          product_id: productId1,
          name: "warna putih",
          buy_price: 8e4,
          sell_price: 9e4,
          sku: "muslim001"
        },
        {
          product_id: productId1,
          name: "warna hitam",
          buy_price: 85e3,
          sell_price: 95e3,
          sku: "muslim002"
        },
        {
          product_id: productId2,
          name: "warna kuning",
          buy_price: 2e5,
          sell_price: 25e4,
          sku: "muslim002"
        },
        {
          product_id: productId3,
          name: "warna abu-abu",
          buy_price: 1e5,
          sell_price: 15e4,
          sku: "batik001"
        }
      ]);
    });

    connection.close();
  } catch (err) {
    throw err;
  }
})();

const getAllProducts = "get all products";

async function cache(_, res, next) {
  const getCache = require("util")
    .promisify(client.get)
    .bind(client);
  const gotCache = await getCache(getAllProducts).catch(err => next(err));

  if (gotCache) {
    return res.json(JSON.parse(gotCache));
  } else {
    return next();
  }
}

app.get("/products", cache, async (_, res, next) => {
  try {
    const connection = await MongoClient.connect(url, {
      useUnifiedTopology: true
    });
    const db = connection.db(dbName);
    const response = await db
      .collection(products)
      .aggregate([
        {
          $match: {
            status: true
          }
        },
        {
          $lookup: {
            from: "variants",
            localField: "_id",
            foreignField: "product_id",
            as: "variants"
          }
        }
      ])
      .toArray();

    client.setex(getAllProducts, 3600, JSON.stringify(response));
    connection.close();
    return res.json(response);
  } catch (err) {
    return next(err);
  }
});

app.get("/remove-status-false", async (_, res, next) => {
  try {
    const connection = await MongoClient.connect(url, {
      useUnifiedTopology: true
    });
    const db = connection.db(dbName);
    const response = await db.collection(products).deleteOne({
      uid: "dn3",
      status: false
    });

    connection.close();
    return res.json(response);
  } catch (err) {
    return next(err);
  }
});

app.listen(3000, () => console.log("Listening on port 3000"));
