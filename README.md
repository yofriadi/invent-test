## Requirement
1. nodejs
2. mongodb
3. redis

## Running

1. npm install
2. node src/

## Endpoint
| route                | description
| -------------------- | ------------------------------------- |
| /product             | list all products with status true    |
| /remove-status-false | remove all products with status false |
